import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import "dotenv/config";
import { routes } from "./api/routes/routes"; //importing route

const HTTP_PORT = process.env.HTTP_PORT || 3001;
const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

routes(app); //register the route

app.listen(HTTP_PORT);

console.log("todo list RESTful API server started on: " + HTTP_PORT);
