import { round } from "./utils";
import { STANDARD_AD, FEATURE_AD, PREMIUM_AD } from "./constant";
import { CompanyDiscountPrice } from "./database";

export const Rules = {
  calculateUEM: (itemCount, adType, price) => {
    if (itemCount >= 3 && adType === STANDARD_AD) {
      const leftOver = itemCount % 3;
      const times = Math.floor(itemCount / 3);

      return round(price * 2 * times + leftOver * price);
    }

    return round(price * itemCount);
  },

  calculateSimeDarby: (itemCount, adType, price) => {
    if (adType === FEATURE_AD) {
      return round(CompanyDiscountPrice[0].discountPrice * itemCount); // 299.99
    }

    return round(price * itemCount);
  },

  calculateIGB: (itemCount, adType, price) => {
    if (adType === PREMIUM_AD && itemCount >= 4) {
      return round(CompanyDiscountPrice[1].discountPrice) * itemCount; // 379.99
    }

    return round(price * itemCount);
  },

  calculateMahSing: (itemCount, adType, price) => {
    if (itemCount >= 5 && adType === STANDARD_AD) {
      const leftOver = itemCount % 5;
      const times = Math.floor(itemCount / 5);

      return round(price * 4 * times + leftOver * price);
    }

    if (adType === FEATURE_AD) {
      return round(CompanyDiscountPrice[2].discountPrice * itemCount); // 309.99
    }

    if (itemCount >= 3 && adType === PREMIUM_AD) {
      return round(CompanyDiscountPrice[3].discountPrice * itemCount); // 389.99
    }

    return round(price * itemCount);
  },

  calculateDefault: (itemCount, adType, price) => {
    return round(price * itemCount);
  },
};
