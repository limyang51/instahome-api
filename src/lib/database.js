import {
  STANDARD_AD,
  FEATURE_AD,
  PREMIUM_AD,
  SIME_DARBY,
  UEM_SUNRISE,
  IGB_BERHAD,
  MAH_SING,
  TRINITY,
  EXISM,
  LBS,
} from "./constant";
import { Advertisement, Customer } from "../model";

export const customers = [
  new Customer({
    name: UEM_SUNRISE,
    priceRule: "calculateUEM",
  }),
  new Customer({
    name: SIME_DARBY,
    priceRule: "calculateSimeDarby",
  }),
  new Customer({
    name: IGB_BERHAD,
    priceRule: "calculateIGB",
  }),
  new Customer({
    name: MAH_SING,
    priceRule: "calculateMahSing",
  }),

  new Customer({
    name: TRINITY,
    priceRule: "calculateDefault",
  }),

  new Customer({
    name: EXISM,
    priceRule: "calculateDefault",
  }),

  new Customer({
    name: LBS,
    priceRule: "calculateDefault",
  }),
];

export const adsType = [
  new Advertisement({
    adType: STANDARD_AD,
    price: 269.99,
  }),
  new Advertisement({
    adType: FEATURE_AD,
    price: 322.99,
  }),
  new Advertisement({
    adType: PREMIUM_AD,
    price: 394.99,
  }),
];

export const CompanyDiscountPrice = [
  {
    Customer: SIME_DARBY,
    adType: STANDARD_AD,
    discountPrice: 299.99,
  },
  {
    Customer: IGB_BERHAD,
    adType: FEATURE_AD,
    discountPrice: 379.99,
  },
  {
    Customer: MAH_SING,
    adType: FEATURE_AD,
    discountPrice: 309.99,
  },
  {
    Customer: MAH_SING,
    adType: PREMIUM_AD,
    discountPrice: 389.99,
  },
];
