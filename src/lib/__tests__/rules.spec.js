import { Rules } from "../rules";
import { STANDARD_AD, FEATURE_AD, PREMIUM_AD } from "../constant";

jest.mock("../database", () => ({
  CompanyDiscountPrice: [
    {
      Customer: "Customer Simer Darby",
      adType: "Standard",
      discountPrice: 50,
    },
    {
      Customer: "Customer IGB",
      adType: "Feature",
      discountPrice: 50,
    },
    {
      Customer: "Customer Mah Sing",
      adType: "Feature",
      discountPrice: 50,
    },
    {
      Customer: "Customer Mah Sing",
      adType: "Premium",
      discountPrice: 50,
    },
  ],
}));

describe("Rule calculation", () => {
  beforeAll(() => {
    jest.resetAllMocks();
  });

  describe("test calculate discount price UEM", () => {
    test("with quantity 2 standard ad", () => {
      const result = Rules.calculateUEM(2, STANDARD_AD, 100);
      expect(result).toBe(200);
    });

    test("with quantity 3 standard ad", () => {
      const result = Rules.calculateUEM(3, STANDARD_AD, 100);
      expect(result).toBe(200);
    });

    test("with quantity 4 standard ad", () => {
      const result = Rules.calculateUEM(4, STANDARD_AD, 100);
      expect(result).toBe(300);
    });

    test("with quantity 5 standard ad", () => {
      const result = Rules.calculateUEM(5, STANDARD_AD, 100);
      expect(result).toBe(400);
    });

    test("with quantity 6 standard ad", () => {
      const result = Rules.calculateUEM(6, STANDARD_AD, 100);
      expect(result).toBe(400);
    });

    test("with quantity 6 Feature ad", () => {
      const result = Rules.calculateUEM(6, FEATURE_AD, 100);
      expect(result).toBe(600);
    });

    test("with quantity 6 Premiumn ad", () => {
      const result = Rules.calculateUEM(6, PREMIUM_AD, 100);
      expect(result).toBe(600);
    });
  });

  describe("test calculate discount price Sime Darby", () => {
    test("with quantity 2 feature ad", () => {
      const result = Rules.calculateSimeDarby(2, FEATURE_AD, 100);
      expect(result).toBe(100);
    });

    test("with quantity 3 feature ad", () => {
      const result = Rules.calculateSimeDarby(3, FEATURE_AD, 100);
      expect(result).toBe(150);
    });

    test("with quantity 4 feature ad", () => {
      const result = Rules.calculateSimeDarby(4, FEATURE_AD, 100);
      expect(result).toBe(200);
    });

    test("with quantity 5 standard ad", () => {
      const result = Rules.calculateSimeDarby(5, STANDARD_AD, 100);
      expect(result).toBe(500);
    });

    test("with quantity 6 premiumn ad", () => {
      const result = Rules.calculateSimeDarby(6, PREMIUM_AD, 100);
      expect(result).toBe(600);
    });
  });

  describe("test calculate discount price IGB", () => {
    test("with quantity 3 premiumn ad", () => {
      const result = Rules.calculateIGB(3, PREMIUM_AD, 100);
      expect(result).toBe(300);
    });

    test("with quantity 4 premiumn ad", () => {
      const result = Rules.calculateIGB(4, PREMIUM_AD, 100);
      expect(result).toBe(200);
    });

    test("with quantity 5 premiumn ad", () => {
      const result = Rules.calculateIGB(5, PREMIUM_AD, 100);
      expect(result).toBe(250);
    });

    test("with quantity 6 premiumn ad", () => {
      const result = Rules.calculateIGB(6, PREMIUM_AD, 100);
      expect(result).toBe(300);
    });

    test("with quantity 6 feature ad", () => {
      const result = Rules.calculateIGB(6, FEATURE_AD, 100);
      expect(result).toBe(600);
    });

    test("with quantity 6 premiumn ad", () => {
      const result = Rules.calculateIGB(6, STANDARD_AD, 100);
      expect(result).toBe(600);
    });
  });

  describe("test calculate discount price Mah Sing", () => {
    test("with quantity 4 standard ad", () => {
      const result = Rules.calculateMahSing(4, STANDARD_AD, 100);
      expect(result).toBe(400);
    });

    test("with quantity 5 standard ad", () => {
      const result = Rules.calculateMahSing(5, STANDARD_AD, 100);
      expect(result).toBe(400);
    });

    test("with quantity 6 standard ad", () => {
      const result = Rules.calculateMahSing(6, STANDARD_AD, 100);
      expect(result).toBe(500);
    });

    test("with quantity 3 feature ad", () => {
      const result = Rules.calculateMahSing(3, FEATURE_AD, 100);
      expect(result).toBe(150);
    });

    test("with quantity 4 feature ad", () => {
      const result = Rules.calculateMahSing(4, FEATURE_AD, 100);
      expect(result).toBe(200);
    });

    test("with quantity 5 feature ad", () => {
      const result = Rules.calculateMahSing(5, FEATURE_AD, 100);
      expect(result).toBe(250);
    });

    test("with quantity 2 premiumn ad", () => {
      const result = Rules.calculateMahSing(2, PREMIUM_AD, 100);
      expect(result).toBe(200);
    });

    test("with quantity 3 premiumn ad", () => {
      const result = Rules.calculateMahSing(3, PREMIUM_AD, 100);
      expect(result).toBe(150);
    });

    test("with quantity 4 premiumn ad", () => {
      const result = Rules.calculateMahSing(4, PREMIUM_AD, 100);
      expect(result).toBe(200);
    });
  });

  describe("test calculate discount price normal company", () => {
    test("with quantity 6 standard ad", () => {
      const result = Rules.calculateDefault(6, STANDARD_AD, 100);
      expect(result).toBe(600);
    });

    test("with quantity 6 feature ad", () => {
      const result = Rules.calculateDefault(6, FEATURE_AD, 100);
      expect(result).toBe(600);
    });

    test("with quantity 6 premumn ad", () => {
      const result = Rules.calculateDefault(6, PREMIUM_AD, 100);
      expect(result).toBe(600);
    });
  });
});
