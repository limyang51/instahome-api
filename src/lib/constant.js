// Ad Type
export const STANDARD_AD = "Standard";
export const FEATURE_AD = "Feature";
export const PREMIUM_AD = "Premium";

// Company
export const SIME_DARBY = "Sime Darby";
export const UEM_SUNRISE = "UEM Sunrise";
export const IGB_BERHAD = "IGB Berhad";
export const MAH_SING = "Mah Sing Group";

// Default
export const TRINITY = "Trinity Group";
export const EXISM = "Exism";
export const LBS = "LBS Property";
