export const mockSend = jest.fn();

export function mockReq({
  body = {},
  headers = {},
  query = null,
  path = null,
} = {}) {
  return {
    body,
    headers,
    query,
    path,
  };
}

export const mockRes = {
  send: function (params) {
    return mockSend(params);
  },
};
