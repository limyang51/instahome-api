import { listAds } from "./index";
import { mockReq, mockRes, mockSend } from "../../../lib/test-utils";
import { adsType } from "../../../lib/database";

describe("listing advertisement", () => {
  beforeAll(() => {
    jest.resetAllMocks();
    listAds(mockReq(), mockRes);
  });

  it("should return list of ads", () => {
    expect(mockSend).toHaveBeenCalledWith({ adsType });
  });
});
