import { adsType, customers } from "../../../lib/database";
import { Rules } from "../../../lib/rules";

export const checkout = async (req, res) => {
  const { customer, adIds } = req.body;

  const items = adIds.map(({ adId, quantity }) => {
    const ad = adsType.find(({ adType }) => adType === adId);

    return {
      advertisement: ad,
      quantity,
    };
  });

  const c = customers.find((c) => c.name === customer);

  const adPrice = items.map(
    ({ advertisement: { adType, price }, quantity }) => {
      const totalPrice = Rules[c.priceRule](quantity, adType, price);
      const discount = quantity * price - totalPrice;
      return {
        ad: adType,
        quantity,
        price,
        totalPrice,
        discount,
      };
    }
  );

  res.send({
    customer: c.name,
    ads: adPrice,
  });
};
