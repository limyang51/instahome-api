import { checkout } from "./index";
import { mockReq, mockRes, mockSend } from "../../../lib/test-utils";

jest.mock("../../../lib/database", () => ({
  customers: [{ name: "Test company", priceRule: "calculateTestPriceRule" }],
  adsType: [{ adType: "Test Ad", price: 100 }],
}));

const mockTestRule = jest.fn();

jest.mock("../../../lib/rules", () => ({
  Rules: {
    calculateTestPriceRule: (params) => mockTestRule(params),
  },
}));

const mockData = {
  customer: "Test company",
  adIds: [{ adId: "Test Ad", quantity: 2 }],
};

describe("Checkout customer bill", () => {
  beforeAll(() => {
    jest.resetAllMocks();
    mockTestRule.mockReturnValue(200);
    checkout(mockReq({ body: mockData }), mockRes);
  });

  it("should return a customer with its calculated price", () => {
    expect(mockSend).toHaveBeenCalledWith({
      customer: "Test company",
      ads: [
        {
          ad: "Test Ad",
          quantity: 2,
          price: 100,
          totalPrice: 200,
          discount: 0,
        },
      ],
    });
  });
});
