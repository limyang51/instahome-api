import { listCustomer } from "./index";
import { mockReq, mockRes, mockSend } from "../../../lib/test-utils";

jest.mock("../../../lib/database", () => ({
  customers: [{ name: "Test company", priceRule: "Test Price Rule" }],
}));

describe("listing Customer", () => {
  beforeAll(() => {
    jest.resetAllMocks();

    listCustomer(mockReq(), mockRes);
  });

  it("should return list of customer with name only", () => {
    expect(mockSend).toHaveBeenCalledWith({
      customers: ["Test company"],
    });
  });
});
