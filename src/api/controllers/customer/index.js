import { customers } from "../../../lib/database";

export const listCustomer = async (req, res) => {
  res.send({
    customers: customers.map((c) => c.name),
  });
};
