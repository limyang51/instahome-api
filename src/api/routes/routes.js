import { checkout, listAds, listCustomer } from "../controllers";

export const routes = (app) => {
  app.route("/checkout").post(checkout);

  app.route("/list/ads").get(listAds);
  app.route("/list/customer").get(listCustomer);
};
