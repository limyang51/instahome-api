class Order {
  constructor({ customer, adIds = [], totalAmount }) {
    this.customer = customer;
    this.adIds = adIds;
    this.totalAmount = totalAmount;
  }
}

export default Order;
