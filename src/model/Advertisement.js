class Advertisement {
  constructor({ adType, price }) {
    this.adType = adType;
    this.price = price;
  }
}

export default Advertisement;
