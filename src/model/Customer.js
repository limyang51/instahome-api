class Customer {
  constructor({ name, priceRule = "default" }) {
    this.name = name;
    this.priceRule = priceRule;
  }
}

export default Customer;
