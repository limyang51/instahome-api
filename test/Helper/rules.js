import { round } from "./utils";
import { STANDARD_AD, FEATURE_AD, PREMIUM_AD } from "./constant";

export const Rules = {
  discountUEM: (itemCount, adType, price) => {
    if (itemCount >= 3 && adType === STANDARD_AD) {
      const leftOver = itemCount % 3;
      const times = Math.floor(itemCount / 3);

      return round(price * 2 * times + leftOver * price);
    }

    return round(price * itemCount);
  },

  discountSimeDarby: (itemCount, adType, price) => {
    if (adType === FEATURE_AD) {
      return round(299.99 * itemCount);
    }

    return round(price);
  },

  discountIGB: (itemCount, adType, price) => {
    if (adType === PREMIUM_AD && itemCount >= 4) {
      return round(379.99);
    }

    return price;
  },

  discountMahSing: (itemCount, adType, price) => {
    if (itemCount >= 5 && adType === STANDARD_AD) {
      const leftOver = itemCount % 5;
      const times = Math.floor(itemCount / 5);

      return round(price * 4 * times + leftOver * price);
    }

    if (adType === FEATURE_AD) {
      return round(309.99 * itemCount);
    }

    if (itemCount >= 3 && adType === PREMIUM_AD) {
      return round(389.99 * itemCount);
    }

    return round(price * itemCount);
  },
};
