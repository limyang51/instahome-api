import {
  STANDARD_AD,
  FEATURE_AD,
  PREMIUM_AD,
  SIME_DARBY,
  UEM_SUNRISE,
  IGB_BERHAD,
  MAH_SING,
} from "./constant";
import { Advertisement, Customer } from "../Model";
import { Rules } from "./rules";

export const customers = [
  new Customer({
    name: UEM_SUNRISE,
    discount: Rules.discountUEM,
  }),
  new Customer({
    name: SIME_DARBY,
    discount: Rules.discountSimeDarby,
  }),
  new Customer({
    name: IGB_BERHAD,
    discount: Rules.discountIGB,
  }),
  new Customer({
    name: MAH_SING,
    discount: Rules.discountMahSing,
  }),
];

export const adsType = [
  new Advertisement({
    adType: STANDARD_AD,
    price: 269.99,
  }),
  new Advertisement({
    adType: FEATURE_AD,
    price: 322.99,
  }),
  new Advertisement({
    adType: PREMIUM_AD,
    price: 394.99,
  }),
];
