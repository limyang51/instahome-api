// Ad Type
export const STANDARD_AD = "Standard";
export const FEATURE_AD = "Feature";
export const PREMIUM_AD = "Premium";

export const DEFAULT = "Default";
export const SIME_DARBY = "Sime Darby";
export const UEM_SUNRISE = "UEM Sunrise";
export const IGB_BERHAD = "IGB Berhad";
export const MAH_SING = "Mah Sing Group";
