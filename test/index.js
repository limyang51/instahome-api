import {
  STANDARD_AD,
  FEATURE_AD,
  PREMIUM_AD,
  SIME_DARBY,
} from "./Helper/constant";
import { Order, Item } from "./Model";
import { adsType, customers } from "./Helper/database";

function main() {
  const order = new Order({
    customer: SIME_DARBY,
    adIds: [
      new Item({
        adId: STANDARD_AD,
        quantity: 1,
      }),
      new Item({
        adId: FEATURE_AD,
        quantity: 2,
      }),
    ],
  });

  const { customer, adIds } = order;

  const items = adIds.map(({ adId, quantity }) => {
    const ad = adsType.find(({ adType }) => adType === adId);

    return {
      advertisement: ad,
      quantity,
    };
  });

  const c = customers.find((c) => c.name === customer);
  const adPrice = items.map(
    ({ advertisement: { adType, price }, quantity }) => {
      const totalPrice = c.discount(quantity, adType, price);

      return {
        adType,
        quantity,
        totalPrice,
      };
    }
  );

  console.log(adPrice);
}

main();
