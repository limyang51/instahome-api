class Item {
  constructor({ adId, quantity }) {
    this.adId = adId;
    this.quantity = quantity;
  }
}

export default Item;
