class Customer {
  constructor({ name = "Default", discount = () => {} }) {
    this.name = name;
    this.discount = discount;
  }
}

export default Customer;
