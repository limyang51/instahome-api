const path = require("path");
const nodeExternals = require("webpack-node-externals");

module.exports = {
  entry: "./src/index.js",
  target: "node",
  devtool: "source-map",
  externals: [nodeExternals()],
  mode: process.env.NODE_ENV ? "development" : "production",
  resolve: {
    alias: {
      Lib: path.resolve(__dirname, "src/lib/"),
      Model: path.resolve(__dirname, "src/model/"),
    },
  },
  module: {
    rules: [
      // {
      //   enforce: 'pre',
      //   test: /\.js$/,
      //   exclude: /node_modules/,
      //   loader: 'eslint-loader'
      // },
      {
        test: /\.js$/,
        loaders: ["babel-loader"],
        include: path.join(__dirname, "src"),
        exclude: /node_modules/,
      },
    ],
  },
  output: {
    libraryTarget: "umd",
    path: path.join(__dirname, "dist"),
    filename: "[name].js",
  },
};
